package io.ta.datamanagement.dataplatform.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;

public class DynamicRouteBuilder extends RouteBuilder {

    public static final String MQTT = "mqtt:";

    public static final String ACTIVEMQ = "activemq:queue:";

    private static final String FROM = "measuredData";

    private static final String HELLO_FROM_CAMEL = "Hello from Camel";

    private static final String LOG = "log:";

    private static String MOCK_DEVICE = "timer:bar";

    private final String to;

    public DynamicRouteBuilder(CamelContext context, String to) {
        super(context);
        this.to = to;
    }

    @Override
    public void configure() throws Exception {
        from(MQTT + "device?subscribeTopicName=device.mqtt.topic")
                .log(body().toString())
                // .to(LOG + to)
                .to(MQTT + "newTest?publishTopicName=" + to + ".mqtt.topic");

        from(MOCK_DEVICE)
                .setBody(constant(HELLO_FROM_CAMEL))
                .fil
                .to(MQTT + "device?publishTopicName=device.mqtt.topic");
        //?publishTopicName=mqtt.topic
    }

}
